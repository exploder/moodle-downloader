import requests
from bs4 import BeautifulSoup
from time import sleep
import re
from ftfy import fix_text
import os
from pathlib import Path
from urllib.parse import urlparse
from urllib.parse import urljoin
import json
import sys
import html
import urllib
from getpass import getpass
import pickle
import unicodedata

KEY_ID = 'id'
KEY_ETAG = 'etag'
KEY_FILE_NAME = 'filename'
KEY_SAVED_FILE_NAME = 'finalfilename'
KEY_URL = 'url'


def get_file_id_from_link(address):
    return re.findall(r'[?]id=(\d+)', address)[0]


def has_key_with_value(key, value):
    for f in all_files:
        if f[key] == value:
            return True


def get_element_with_key_value(key, value):
    for f in all_files:
        if f[key] == value:
            return f


def indent_print(*args, **kwargs):
    global indent_level
    print(indent_level * "    " + " ".join(map(str, args)), **kwargs)


def debug_print(*args, **kwargs):
    if DEBUG:
        indent_print('DEBUG: ' + " ".join(map(str, args)), **kwargs)


def contains_mod_folder(href):
    if href is not None:
        return '/mod_folder/content' in href
    return False


def login(url):
    indent_print('User not logged into Moodle, checking IdP')
    resp = session.get(url)
    soup = BeautifulSoup(resp.text, features="html.parser")
    if soup.title is not None and soup.title.text == "TUNI Web Login":
        indent_print('Login needed!')
        login_form = soup.find('form')
        post_url = login_form.attrs['action']
        username = input('Username: ')
        password = getpass('Password: ')
        post_data = {'j_username': username,
                     'j_password': password,
                     '_eventId_proceed': ''}
        resp = session.post(urljoin('https://idp.tuni.fi/', post_url),
                            post_data, allow_redirects=True)
        if 'was incorrect.' in resp.text:
            print('Incorrect username or password! Try again!')
            return login(url)
        elif 'stale request' in resp.text.lower():
            print('Stale request! Try again!')
            return login(url)
        soup = BeautifulSoup(resp.text, features='html.parser')
    continue_form = soup.find('form')
    relay_state = continue_form.find('input',
                                     {'name': 'RelayState'}).get('value')
    saml_resp = continue_form.find('input',
                                   {'name': 'SAMLResponse'}).get('value')
    post_url = continue_form.get('action')
    post_data = {'RelayState': relay_state, 'SAMLResponse': saml_resp}
    resp = session.post(post_url, post_data, allow_redirects=True)
    return resp


def download_id_files(id_links):
    for element in id_links:
        if 'onclick' in element.attrs and element.attrs['onclick'] != "":
            href = re.findall(r"window.open[(]'(.+?)'",
                              element.attrs['onclick'])[0]
            href = html.unescape(href)
        else:
            href = element.attrs['href']

        fileid = get_file_id_from_link(href)
        if has_key_with_value(KEY_ID, fileid):
            add_file = False
            this_file = get_element_with_key_value(KEY_ID, fileid)
        else:
            add_file = True
            this_file = {}

        sleep(SLEEP_TIME)
        if has_key_with_value(KEY_ID, fileid):
            resp = session.head(href, allow_redirects=True)
            if 'Etag' not in resp.headers:
                indent_print(f'No Etag field found, skipping file ID = '
                             f'{fileid}')
                continue

            remote_etag = resp.headers['Etag']
            saved_etag = this_file[KEY_ETAG]
            if remote_etag == saved_etag:
                indent_print('Skipped file ID =', fileid,
                             'because already in downloaded.json')
                continue

        indent_print('Downloading from', href)
        resp = session.get(href, allow_redirects=True)
        if 'text/html' in resp.headers['content-type']:
            soup = BeautifulSoup(resp.text, 'html.parser')
            if soup.title is not None:
                if 'Virhe' in soup.title.text or 'Error' in soup.title.text:
                    indent_print(f'Error! Broken link {href}')
                    continue

        if 'content-disposition' not in resp.headers:
            indent_print(f'content-disposition header not found, skipping '
                         f'file ID = {fileid}')
            continue

        this_file[KEY_ID] = fileid
        this_file[KEY_ETAG] = resp.headers['Etag']
        this_file[KEY_URL] = href

        heads = resp.headers
        fname = urllib.parse.unquote(html.unescape(
            re.findall('filename="(.+)"', heads['content-disposition'])[0]))

        fname = fix_text(fname)
        indent_print('Fixed filename:', fname)
        filename, extension = os.path.splitext(fname)
        this_file[KEY_FILE_NAME] = fname

        files = [unicodedata.normalize('NFC', filu)
                 for filu in os.listdir(current_dir)]

        num = 1
        while fname in files:
            fname = filename + '(' + str(num) + ')' + extension
            num += 1

        indent_print('Final filename:', fname)
        this_file[KEY_SAVED_FILE_NAME] = fname

        with open(current_dir + '/' + fname, 'wb') as out_file2:
            out_file2.write(resp.content)
        if add_file:
            all_files.append(this_file)


def download_other_files(other_links):
    for element in other_links:
        href = element.attrs['href']
        if has_key_with_value(KEY_URL, href):
            this_file = get_element_with_key_value(KEY_URL, href)
        else:
            this_file = {}

        if has_key_with_value(KEY_URL, href):
            indent_print('Skipped file with URL =', href,
                         'because already in downloaded.json')
            continue

        indent_print('Downloading from', href)
        sleep(SLEEP_TIME)
        resp = session.get(href, allow_redirects=True)
        if 'text/html' in resp.headers['content-type']:
            soup = BeautifulSoup(resp.text, 'html.parser')
            if soup.title is not None:
                if 'Virhe' in soup.title.text or 'Error' in soup.title.text:
                    indent_print(f'Error! Broken link {href}')
                    continue

        this_file[KEY_ID] = ''
        this_file[KEY_ETAG] = resp.headers['Etag']
        this_file[KEY_URL] = href

        fname = urllib.parse.unquote(html.unescape(
            os.path.basename(urlparse(href).path)))

        fname = fix_text(fname)
        indent_print('Fixed filename:', fname)
        filename, extension = os.path.splitext(fname)
        this_file[KEY_FILE_NAME] = fname

        files = [unicodedata.normalize('NFC', filu)
                 for filu in os.listdir(current_dir)]

        num = 1
        while fname in files:
            fname = filename + '(' + str(num) + ')' + extension
            num += 1

        indent_print('Final filename:', fname)
        this_file[KEY_SAVED_FILE_NAME] = fname

        with open(current_dir + '/' + fname, 'wb') as out_file2:
            out_file2.write(resp.content)
        all_files.append(this_file)


def download_folder(page_url):
    global location
    global indent_level
    indent_level += 1
    indent_print('Entering', page_url)

    r = session.get(page_url)
    soup = BeautifulSoup(r.text, features='html.parser')
    forms = soup.find_all('form')
    download_form = None
    for form in forms:
        if 'download_folder.php' in form.attrs.get('action'):
            download_form = form
    if download_form is not None:
        indent_print('Form for downloading a zip found!')
        indent_print('Checking whether this file has already '
                     'been downloaded...')

        links = soup.find_all('a', href=contains_mod_folder,
                              title=False)
        download_zip = False
        for link in links:
            href = link.attrs['href']
            if has_key_with_value(KEY_URL, href):
                indent_print('File with URL =', href,
                             'has already been downloaded.')

                continue
            else:
                download_zip = True
                break

        if download_zip:
            indent_print('Downloading zip...')
            session_key = \
                download_form.find('input', {'name': 'sesskey'}).attrs['value']
            folder_id = download_form.find('input',
                                           {'name': 'id'}).attrs['value']

            form_data = {'id': folder_id, 'sesskey': session_key}
            if location == 'tut':
                resp = session.post('https://moodle2.tut.fi/'
                                    'mod/folder/download_folder.php',
                                    form_data)
            elif location == 'tuni':
                resp = session.post('https://moodle.tuni.fi/'
                                    'mod/folder/download_folder.php',
                                    form_data)

            heads = resp.headers

            fname = urllib.parse.unquote(html.unescape(
                re.findall('filename="(.+)"',
                           heads['content-disposition'])[0]))

            indent_print('Zip filename:', fname)
            fname = fix_text(fname)
            indent_print('Fized filename:', fname)

            filename, extension = os.path.splitext(fname)

            files = [unicodedata.normalize('NFC', filu)
                     for filu in os.listdir(current_dir)]

            num = 1
            while fname in files:
                fname = filename + '(' + str(num) + ')' + extension
                num += 1

            indent_print('Final filename:', fname)

            with open(current_dir + '/' + fname, 'wb') as out_file2:
                out_file2.write(resp.content)

            indent_print('Zip downloaded! '
                         'Adding file info to downloaded.json...')

            for link in links:
                href = link.attrs['href']
                sleep(SLEEP_TIME)

                if not has_key_with_value(KEY_URL, href):
                    this_file = {KEY_URL: href, KEY_ID: '',
                                 KEY_FILE_NAME: '', KEY_SAVED_FILE_NAME: ''}
                    resp = session.head(href, allow_redirects=True)
                    this_file[KEY_ETAG] = resp.headers['Etag']
                    all_files.append(this_file)
        else:
            indent_print('A zip containing all files '
                         'has already been downloaded!')

    else:
        indent_print('No download files form found, downloading '
                     'individually...')
        links = soup.find_all('a', href=True, title=False)
        id_links = []
        other_links = []
        indent_print('Finding links...')
        for link in links:
            if '/mod/resource/view.php' in link.attrs['href']:
                indent_print('Found link', link.attrs['href'])
                id_links.append(link)
            elif '/pluginfile.php/' in link.attrs['href']:
                indent_print('Found link', link.attrs['href'])
                other_links.append(link)

        download_id_files(id_links)

        download_other_files(other_links)

    indent_print('Exiting', page_url)
    indent_level -= 1


def download_files(page_url):
    global location
    global indent_level
    indent_level += 1
    indent_print('Entering', page_url)

    sleep(SLEEP_TIME)
    r = session.get(page_url)

    # Haetaan kauniilla keitolla (hehe) linkit
    soup = BeautifulSoup(r.text, features="html.parser")
    if 'Kirjaudu sivustoon' in soup.title.text:
        if location == 'tut':
            login_element = soup.find('div',
                                      id='login-buttons-container').find('a')
        elif location == 'tuni':
            login_element = soup.find('div',
                                      {'class':
                                       'login-content-holder'})\
                .find('a', {'class': 'btn'})
        login_link = login_element.attrs['href']
        resp = login(login_link)
        soup = BeautifulSoup(resp.text, features='html.parser')

    links = soup.find('div',
                      id='content' if location == 'tut'
                      else 'page-content').find_all('a',
                                                    href=True, title=False)

    # Haetaan linkeistä pdf (noh, "resurssi") -linkit
    id_links = []
    other_links = []

    indent_print('Finding links...')
    for link in links:
        if '/mod/resource/view.php' in link.attrs['href']:
            indent_print('Found link', link.attrs['href'])
            id_links.append(link)
        elif '/pluginfile.php/' in link.attrs['href']:
            indent_print('Found link', link.attrs['href'])
            other_links.append(link)
        elif '/mod/page/view.php' in link.attrs['href'] and \
                '/mod/page/view.php' not in page_url:
            download_files(link.attrs['href'])
        elif '/mod/folder/view.php' in link.attrs['href']:
            download_folder(link.attrs['href'])

    download_id_files(id_links)

    download_other_files(other_links)
    indent_print('Exiting', page_url)
    indent_level -= 1


if __name__ == '__main__':
    DEBUG = True
    SLEEP_TIME = 1
    indent_level = 0
    script_dir = sys.path[0]
    if len(sys.argv) < 3:
        save_path = Path('course.txt')
        if not save_path.is_file():
            indent_print('Too few arguments. '
                         'Pass the course ID and course location (tuni/tut) '
                         'as an argument.')
            sys.exit(1)
        else:
            with open(save_path, 'r') as save_file:
                course_id = save_file.readline()
                location = save_file.readline()
                if location == '':
                    location = 'tut'
    else:
        course_id = sys.argv[1]
        location = sys.argv[2]
        if location not in ['tuni', 'tut']:
            indent_print('Illegal argument, location must be either '
                         'tut or tuni')
            sys.exit(1)
        with open('course.txt', 'w') as save_file:
            save_file.write(f'{course_id}\n{location}')

    current_dir = os.getcwd()

    all_files = []
    download_filename = 'downloaded.json'
    if download_filename in os.listdir(current_dir):
        all_files = json.load(open(current_dir + '/' + download_filename))

    # Requestsin Sessio
    session = requests.Session()

    # Ladataan keksit
    cookie_save_path = Path(script_dir, 'cookies.bin')
    if cookie_save_path.is_file():
        with open(cookie_save_path, 'rb') as cookie_file:
            print('Loading cookies!')
            session.cookies.update(pickle.load(cookie_file))

    session.headers.update({'User-Agent': 'Moodle-Downloader'})

    # Ladataan moodle-sivu
    if location == 'tut':
        download_files('https://moodle2.tut.fi/course/view.php?id=' + course_id)
    elif location == 'tuni':
        download_files('https://moodle.tuni.fi/course/view.php?id=' + course_id)
    indent_print('Saving downloaded history!')
    with open(current_dir + '/' + download_filename, 'w') as out_file:
        json.dump(all_files, out_file)

    indent_print('Saving cookies!')
    with open(cookie_save_path, 'wb') as cookie_file:
        pickle.dump(session.cookies, cookie_file)
